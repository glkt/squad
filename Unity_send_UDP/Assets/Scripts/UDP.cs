using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net;  
using System.Net.Sockets;  
using System.Text; 

public static class UDP{
	
	private static Socket sock;

    static public void init(){        
        sock = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
    }

    static public void send(string IP, int port, string message){	
		IPAddress serverAddr = IPAddress.Parse(IP);
		IPEndPoint endPoint = new IPEndPoint(serverAddr, port);
		byte[] send_buffer = Encoding.ASCII.GetBytes(message);
		sock.SendTo(send_buffer , endPoint);
	}

	static public void sendRaw(string IP, int port, byte[] message){	
		IPAddress serverAddr = IPAddress.Parse(IP);
		IPEndPoint endPoint = new IPEndPoint(serverAddr, port);
		sock.SendTo(message , endPoint);
	}
}
