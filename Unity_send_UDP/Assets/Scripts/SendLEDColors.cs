using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Collections.Generic;
using System.Text;
using System;

public class SendLEDColors : MonoBehaviour
{
	public string ip = "";
	public string bigSquidIp = "";
	public int port = 8888;

	public string[] ips;

	private List<byte> bytesList = new List<byte>();
	private List<byte> bytesListBigSquid = new List<byte>();
	private byte[] bytesToSend;
	public bool sendMsg = false;
	public bool sendOneMsg = false;

	public Color bodyColor;
	public Color finsColor;
	public Color eyesColor;

	public float speed= 5f;

	public bool motorOnOff = false;

	float timer = 0f;
	public float frameRate = 20;

	bool debugLogPacket = false;

    void Start()
    {
        UDP.init();
    }

    void Update()
    {

    	timer += Time.deltaTime;

    	if(timer > (1/frameRate) ){
    		timer = 0;

	        // clear
	        bytesList.Clear();
	        bytesListBigSquid.Clear();

	        // define colors
	        float timeNow = Time.time;
	        timeNow *= speed;

	        // motor On or Off
	        if(motorOnOff){
	        	bytesList.Add( (byte) 1);
	        	bytesListBigSquid.Add( (byte) 1);
	        }else{
	       		bytesList.Add( (byte) 0);
	       		bytesListBigSquid.Add( (byte) 0);
	       	}

	        // body color
	        for(int i = 0; i < 138; i++){
	        	float sine = (Mathf.Sin((float)i/10f+timeNow)+1)*127;

	            bytesList.Add( (byte) (sine * bodyColor.r) );
	            bytesList.Add( (byte) (sine * bodyColor.g) );
	            bytesList.Add( (byte) (sine * bodyColor.b) );
	        }

	        // body color 2
	        for(int i = 0; i < 138; i++){
	        	float sine = (Mathf.Sin((float)i/10f+timeNow)+1)*127;

	            bytesList.Add( (byte) (sine * bodyColor.r) );
	            bytesList.Add( (byte) (sine * bodyColor.g) );
	            bytesList.Add( (byte) (sine * bodyColor.b) );

	        }

	        // fin colors
	        for(int i = 0; i < 98; i++){
	        	float sine = (Mathf.Sin((float)i/10f+timeNow)+1)*127;

	            bytesList.Add( (byte) (sine * finsColor.r) );
	            bytesList.Add( (byte) (sine * finsColor.g) );
	            bytesList.Add( (byte) (sine * finsColor.b) );
	        }

	        // eyes
	        for(int i = 0; i < 90; i++){
	        	float sine = (Mathf.Sin((float)i/10f+timeNow)+1)*127;

	            bytesList.Add( (byte) (sine * eyesColor.r) );
	            bytesList.Add( (byte) (sine * eyesColor.g) );
	            bytesList.Add( (byte) (sine * eyesColor.b) );
	        }

	        // BIG SQUID--------------------------------------------------
	        for(int i = 0; i < 130; i++){
	        	float sine = (Mathf.Sin((float)i/10f+timeNow)+1)*127;
	            bytesListBigSquid.Add( (byte) (sine * bodyColor.r) );
	            bytesListBigSquid.Add( (byte) (sine * bodyColor.g) );
	            bytesListBigSquid.Add( (byte) (sine * bodyColor.b) );
	        }
	        for(int i = 0; i < 130; i++){
	        	float sine = (Mathf.Sin((float)i/10f+timeNow)+1)*127;
	            bytesListBigSquid.Add( (byte) (sine * bodyColor.r) );
	            bytesListBigSquid.Add( (byte) (sine * bodyColor.g) );
	            bytesListBigSquid.Add( (byte) (sine * bodyColor.b) );
	        }
	        for(int i = 0; i < 124; i++){
	        	float sine = (Mathf.Sin((float)i/10f+timeNow)+1)*127;
	            bytesListBigSquid.Add( (byte) (sine * finsColor.r) );
	            bytesListBigSquid.Add( (byte) (sine * finsColor.g) );
	            bytesListBigSquid.Add( (byte) (sine * finsColor.b) );
	        }
	        for(int i = 0; i < 90; i++){
	        	float sine = (Mathf.Sin((float)i/10f+timeNow)+1)*127;
	            bytesListBigSquid.Add( (byte) (sine * eyesColor.r) );
	            bytesListBigSquid.Add( (byte) (sine * eyesColor.g) );
	            bytesListBigSquid.Add( (byte) (sine * eyesColor.b) );
	        }
	        //--------------------------------------------------------------


	        // send data
	        if(sendMsg || sendOneMsg){
	            bytesToSend = bytesList.ToArray();            

	            //send to multiple adresses
	            for(int j = 0; j< ips.Length; j++){
	            	UDP.sendRaw(ips[j], port, bytesToSend);
	            }

	            // send to big squid
	            bytesToSend = bytesListBigSquid.ToArray(); 
	            UDP.sendRaw(bigSquidIp, port, bytesToSend);


	            if(debugLogPacket){
		            String debugMsg = "sent : ";
		            for(int k = 0; k < bytesList.Count; k++){
		            	debugMsg += bytesList[k];
		            	debugMsg += ".";
		            }
		            Debug.Log(debugMsg);
		        }

	      		sendOneMsg = false;

	      	}	
	    }
    }
}
