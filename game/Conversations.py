
class Conversations:

	def __init__(self, in_list = {}, in_mehList = [] ):
		self.list = in_list
		self.meh = in_mehList

	def add(self, key, item):
		self.list[key] = item

	def addMeh(self, item):
		self.meh.append(item)

	def getAllPhrases(self):
		return list(self.list.keys())

	def ask(self, phrase = "" ):
		
		if (phrase == False or len(phrase) == 0) :
			return False

		if phrase in self.list:
			return self.list[phrase]

		else:
			print("phrase doesn't exist in dictionary")
			return False


# ADD "Meh" phrases when player phrase doesn't exist

# conversations.add("hello", { 
# 	"answer": "yo", 
# 	"context": None,
# 	"squidAnimation": 1,
# 	"score": 1
# })