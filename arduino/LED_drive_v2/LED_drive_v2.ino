#include "WiFi.h"
#include "AsyncUDP.h"
#include <FastLED.h>

// this code receive UDP packet that must parsed as follow :
// - first byte set the state of motor : ON or OFF (0 or 1)
// - following 219 bytes are RGB values for 73 pixels for the BODY
// - following 147 bytes are RGB values for 49 pixels for the FINS
// - following 135 bytes are RGB values for 45 pixels for the EYES

// pinout, depends of printed circuit board
#define PIN1 1 // body
#define PIN2 22 // body, 2nd strip, same signal
#define PIN3 23 // fins
#define PIN4 3 // eyes
#define PIN_MOTOR 21 // eyes

#define UDP_TX_PACKET_MAX_SIZE 1000

byte udpBuffer[UDP_TX_PACKET_MAX_SIZE];

// number of pixels per segment (pixels can be repeated and mirrored over led strip, so there might be more physical led)
// this is how much pixels are sent over the network. Multiply by 3 to get byte length
#define bodyLedCOUNT 73
#define finLedCOUNT 49
#define eyeLedCOUNT 45

// pixels are duplicated and mirrored along LED strips
CRGB ledsBody[bodyLedCOUNT*2];
CRGB ledsFin[finLedCOUNT*2];
CRGB ledsEye[eyeLedCOUNT*2];
bool motorState = false;

const char* ssid = "squidsquad";
const char* pass = "fuckyourburn";

const uint bodyDataOffset = bodyLedCOUNT * 3 + 1;
const uint finDataOffset = (bodyLedCOUNT + finLedCOUNT) * 3 + 1;
const uint eyeDataOffset = (bodyLedCOUNT + finLedCOUNT + eyeLedCOUNT) * 3 + 1;

AsyncUDP udp;

// Set your Static IP address
IPAddress local_IP(192, 168, 1, 199);
IPAddress gateway(192, 168, 1, 1);
IPAddress subnet(255, 255, 255, 0);

void setup()
{
  //Serial.begin(115200);

  // Configures static IP address
  if (!WiFi.config(local_IP, gateway, subnet)){
    //Serial.println("STA Failed to configure");
  }
  
  // confirm wifi
  //Serial.print("Connecting to ");
  //Serial.println(ssid);

  // connect
  WiFi.begin(ssid, pass);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    //Serial.print(".");
  }

  pinMode(PIN_MOTOR, OUTPUT);

  // set-up LED
  FastLED.addLeds<WS2812B, PIN1, RGB>(ledsBody, bodyLedCOUNT*2);
  FastLED.addLeds<WS2812B, PIN2, RGB>(ledsBody, bodyLedCOUNT*2);
  FastLED.addLeds<WS2812B, PIN3, RGB>(ledsFin, finLedCOUNT*2);
  FastLED.addLeds<WS2812B, PIN4, GRB>(ledsEye, eyeLedCOUNT*2);
  
  if (udp.listen(1234)) {
    //Serial.print("UDP Listening on IP: ");
    //Serial.println(WiFi.localIP());
    
    udp.onPacket([](AsyncUDPPacket packet) {

      //Serial.println("received packet");
      //packet.printf("%u bytes", packet.length());

      for (int i=0;i<packet.length();i++){

          // motor On or OFF          
          if(i==0){
            byte check = *(packet.data());
            if(check == 0){
              //if(!motorState){
              //  motorState = true;
                digitalWrite(PIN_MOTOR, HIGH);
              //}
            }else{
              //if(motorState){
              //  motorState = false;
                digitalWrite(PIN_MOTOR, LOW);
              //}
            }
          }

          // body LED strip
          else if(i<bodyDataOffset){
            ledsBody[(i-1)/3].setRGB (    *(packet.data()+i),
                                        *(packet.data()+i+1),
                                        *(packet.data()+i+2));
            ledsBody[bodyLedCOUNT*2-1-(i-1)/3].setRGB (  *(packet.data()+i),
                                                       *(packet.data()+i+1),
                                                       *(packet.data()+i+2));
            i+=2;
          }

          // body LED strip
          else if(i<finDataOffset){
            ledsFin[(i-bodyDataOffset)/3].setRGB (  *(packet.data()+i),
                                                    *(packet.data()+i+1),
                                                    *(packet.data()+i+2));
            ledsFin[finLedCOUNT*2-1-(i-bodyDataOffset)/3].setRGB (  *(packet.data()+i),
                                                    *(packet.data()+i+1),
                                                    *(packet.data()+i+2));
            i+=2;
          }

          // eyes LED strips
          else if(i<eyeDataOffset){
            ledsEye[(i-finDataOffset)/3].setRGB ( *(packet.data()+i),
                                                  *(packet.data()+i+1),
                                                  *(packet.data()+i+2));
            ledsEye[eyeLedCOUNT*2-1-(i-finDataOffset)/3].setRGB ( *(packet.data()+i),
                                                  *(packet.data()+i+1),
                                                  *(packet.data()+i+2));
            i+=2;
          }
          
         
      }

      // debug show current led values on packet received.
      //Serial.print("motor state :"); Serial.println(motorState);
    });
  }
}

void loop()
{
  //delay(15);
  FastLED.show();

}
