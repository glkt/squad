#include "WiFi.h"
#include "AsyncUDP.h"
#include <FastLED.h>

// pinout, depends of printed circuit board
#define PIN1 1 // body
#define PIN2 22 // body, 2nd strip, same signal
#define PIN3 23 // fins
#define PIN4 3 // eyes
#define PIN_MOTOR 21 // eyes

#define UDP_TX_PACKET_MAX_SIZE 1450

byte udpBuffer[UDP_TX_PACKET_MAX_SIZE];

// number of pixels per segment (pixels can be repeated and mirrored over led strip, so there might be more physical led)
// this is how much pixels are sent over the network. Multiply by 3 to get byte length
#define bodyLedCOUNT 130
#define finLedCOUNT 124
#define eyeLedCOUNT 90

// pixels are duplicated and mirrored along LED strips
CRGB ledsBody[bodyLedCOUNT];
CRGB ledsBody2[bodyLedCOUNT];
CRGB ledsFin[finLedCOUNT];
CRGB ledsEye[eyeLedCOUNT];
bool motorState = false;

const char* ssid = "squidsquad";
const char* pass = "fuckyourburn";

const uint bodyDataOffset = bodyLedCOUNT * 3 + 1;
const uint bodyDataOffset2 = (bodyLedCOUNT*2) * 3 + 1;
const uint finDataOffset = (bodyLedCOUNT*2 + finLedCOUNT) * 3 + 1;
const uint eyeDataOffset = (bodyLedCOUNT*2 + finLedCOUNT + eyeLedCOUNT) * 3 + 1;

AsyncUDP udp;

// Set your Static IP address
IPAddress local_IP(192, 168, 1, 199);
IPAddress gateway(192, 168, 1, 1);
IPAddress subnet(255, 255, 255, 0);

void setup()
{
  // Configures static IP address
  if (!WiFi.config(local_IP, gateway, subnet)){
    //Serial.println("STA Failed to configure");
  }

  // connect
  WiFi.begin(ssid, pass);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    //Serial.print(".");
  }

  pinMode(PIN_MOTOR, OUTPUT);

  // set-up LED
  FastLED.addLeds<WS2812B, PIN1, RGB>(ledsBody, bodyLedCOUNT);
  FastLED.addLeds<WS2812B, PIN2, RGB>(ledsBody2, bodyLedCOUNT);
  FastLED.addLeds<WS2812B, PIN3, RGB>(ledsFin, finLedCOUNT);
  FastLED.addLeds<WS2812B, PIN4, GRB>(ledsEye, eyeLedCOUNT);
  
  if (udp.listen(1234)) {
    
    udp.onPacket([](AsyncUDPPacket packet) {

      for (int i=0;i<packet.length();i++){

          // motor On or OFF          
          if(i==0){
            byte check = *(packet.data());
            if(check == 0){
                digitalWrite(PIN_MOTOR, HIGH);
            }else{
                digitalWrite(PIN_MOTOR, LOW);
            }
          }

          // body LED strip
          else if(i<bodyDataOffset){
            ledsBody[(i-1)/3].setRGB (  *(packet.data()+i),
                                        *(packet.data()+i+1),
                                        *(packet.data()+i+2));
            i+=2;
          }

          // body second LED strip
          else if(i<bodyDataOffset2){
            ledsBody2[(i-bodyDataOffset)/3].setRGB ( *(packet.data()+i),
                                                     *(packet.data()+i+1),
                                                     *(packet.data()+i+2));
            i+=2;
          }

          // fins LED strip
          else if(i<finDataOffset){
            ledsFin[(i-bodyDataOffset2)/3].setRGB ( *(packet.data()+i),
                                                    *(packet.data()+i+1),
                                                    *(packet.data()+i+2));
            i+=2;
          }

          // eyes LED strips
          else if(i<eyeDataOffset){
            ledsEye[(i-finDataOffset)/3].setRGB ( *(packet.data()+i),
                                                  *(packet.data()+i+1),
                                                  *(packet.data()+i+2));
            i+=2;
          }  
        }
    });
  }
}

void loop()
{
  FastLED.show();
}
