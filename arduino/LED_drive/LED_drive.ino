#include "WiFi.h"
#include "AsyncUDP.h"
#include <FastLED.h>

#define PIN1 1
#define PIN2 22
#define PIN3 23
#define PIN4 3

#define LED_COUNT 300
#define UDP_TX_PACKET_MAX_SIZE 6000
byte udpBuffer[UDP_TX_PACKET_MAX_SIZE];
CRGB leds[LED_COUNT];

const char* ssid = "squidsquad";
const char* pass = "fuckyourburn";

bool ledState = false;

AsyncUDP udp;

// Set your Static IP address
IPAddress local_IP(192, 168, 1, 200);
IPAddress gateway(192, 168, 1, 1);
IPAddress subnet(255, 255, 255, 0);

void setup()
{

  Serial.begin(115200);
 // WiFi.disconnect(true);

  // Configures static IP address
  if (!WiFi.config(local_IP, gateway, subnet)) {
    Serial.println("STA Failed to configure");
  }
  
  // confirm wifi
  Serial.print("Connecting to ");
  Serial.println(ssid);

  // connect
  WiFi.begin(ssid, pass);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  // set-up LED
  FastLED.addLeds<WS2812B, PIN1, GRB>(leds, LED_COUNT);
  FastLED.addLeds<WS2812B, PIN2, GRB>(leds, LED_COUNT);
  FastLED.addLeds<WS2812B, PIN3, GRB>(leds, LED_COUNT);
  FastLED.addLeds<WS2812B, PIN4, GRB>(leds, LED_COUNT);
  
  if (udp.listen(1234)) {
    Serial.print("UDP Listening on IP: ");
    Serial.println(WiFi.localIP());
    udp.onPacket([](AsyncUDPPacket packet) {

      // debug show packet data
      
      Serial.print("UDP Packet Type: ");
      Serial.print(packet.isBroadcast() ? "Broadcast" : packet.isMulticast() ? "Multicast" : "Unicast");
      Serial.print(", From: ");
      Serial.print(packet.remoteIP());
      Serial.print(":");
      Serial.print(packet.remotePort());
      Serial.print(", To: ");
      Serial.print(packet.localIP());
      Serial.print(":");
      Serial.print(packet.localPort());
      Serial.print(", Length: ");
      Serial.print(packet.length()); //dlzka packetu
      Serial.print(", Data: ");
      Serial.write(packet.data(), packet.length());
      Serial.println();      
      packet.printf("Got %u bytes of data", packet.length());

      for (int i=0;i<packet.length()-2;i++){
        
          leds[i/3].setRGB (*(packet.data()+i),
                          *(packet.data()+i+1),
                          *(packet.data()+i+2));
                          
          // debug show led colors data

          if(i==0){
            Serial.print("led-");
            Serial.println(*(packet.data()+i))
            ;
          }
          /*
          Serial.print("led-");
          Serial.print(i/3);
          Serial.print(" r ");
          Serial.print(*(packet.data()+i));
          Serial.print(" g ");
          Serial.print(*(packet.data()+i+1));
          Serial.print(" b ");
          Serial.print(*(packet.data()+i+2));
          Serial.println();
          */
                          
          i+=2;         
      }
    });
  }
}

void loop()
{
  delay(5);
  FastLED.show();
}
