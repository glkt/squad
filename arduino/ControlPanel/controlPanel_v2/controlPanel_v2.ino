// board : arduino mega 
// processor : ATmega 2560

#define BUTTONCOUNT 28

// button and led states
bool btnState[BUTTONCOUNT];
bool ledState[BUTTONCOUNT];

// pinout mapping, using names (written on the board)
//const byte ledPin[] = {A3,A1,A7,A5,A11,A9,A15,A13,D35,D33,D39,D37,D43,D41,D48,D46,D52,D50,D19,D21,D15,D17,D11,D13,D7,D9,D3,D5};
//const byte btnPin[] = {A2,A0,A6,A4,A10,A8,A14,A12,D34,D32,D38,D36,D42,D40,D49,D47,D53,D51,D18,D20,D14,D16,D10,D12,D6,D8,D2,D4};

// pinout mapping, translated to pin number instead of names
// using : https://docs.arduino.cc/hacking/hardware/PinMapping2560
// ///////////////////{A3,A1,A7,A5,A11,A9,A15,A13,D35,D33,D39,D37,D43,D41,D48,D46,D52,D50,D19,D21,D15,D17,D11,D13,D7,D9,D3,D5};

// mapping Pin name to Pin Number (ex: A3 to 57)
const byte ledPin[]= {57,55,61,59,65, 63,69, 67, 35, 33, 39, 37, 43, 41, 48, 46, 52, 50, 19, 21, 15, 17, 11, 13, 7, 9, 3, 5};
const byte btnPin[]= {56,54,60,58, 64,62, 68, 66, 34, 32, 38, 36, 42, 40, 49, 47, 53, 51, 18, 20, 14, 16, 10, 12, 6, 8, 2, 4};

// Infrared sensor on pin D44
#define sensorPin 44
bool sensorState = false;

// chars
const char onC[] =  {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','(','['};
const char offC[] = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',')',']'};

// used to read button one after another
bool currentInput = false;

int findInArrayOn(char c){
  for (int i=0;i<BUTTONCOUNT;i++){
    if(onC[i] == c){
      return i;      
    }
  }
  return -1;
}
int findInArrayOff(char c){
  for (int i=0;i<BUTTONCOUNT;i++){
    if(offC[i] == c){
      return i;
    }
  }
  return -1;
}

void setup() {
  // set up input and output
  for (int i=0;i<BUTTONCOUNT;i++){
    pinMode(ledPin[i], OUTPUT);
    pinMode(btnPin[i], INPUT);
  }

  pinMode(sensorPin, INPUT);

  Serial.begin(115200);

  digitalWrite(A3,HIGH);
}

void loop() {

  // loop through each button, update state if changed and send char to serial
  for (int i=0;i<BUTTONCOUNT;i++){
    // read button
     currentInput = digitalRead (btnPin[i]);
     if( currentInput != btnState[i]){
       btnState[i] = currentInput;
       //Serial.write((currentInput ? onC[i] : offC[i] ));
     }  
  }

  bool currentSensorReading = digitalRead(sensorPin);
  if(currentSensorReading != sensorState) {
    sensorState = currentSensorReading;
    char outChar = sensorState ? '!' : '?' ;
    Serial.write(outChar);
    //Serial.println( sensorState ? "ON" : "OFF");
  }

  // read led state from computer
  if (Serial.available()) {       
    char c = Serial.read();

    int index = findInArrayOn(c);
    bool state = true;
    if( index == -1){
      index = findInArrayOff(c);
      state = false;
    }
    if(index != -1){ // if received char is mapped to an actual buttons led (ignore line return and weird stuff)
      digitalWrite(ledPin[index], state);

      // debug print led status
      /*Serial.print("received : ");
      Serial.print(c);
      Serial.print(" - change led for button ");
      Serial.print(index);
      Serial.print(" to : ");
      Serial.print(state);
      Serial.print(", changed pin : ");
      Serial.println(ledPin[index]);*/
    }
  }
}
